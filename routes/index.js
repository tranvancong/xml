import express from 'express';
import news from '../controllers/news';

const router = express();

router.get('/news', news.getAllNews);

export default router;