import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import express from 'express';
import 'express-async-errors';
import favicon from 'serve-favicon';
import path from 'path';
import { Server } from 'http';
import dotenv from 'dotenv';
import routes from './routes';
import mongoose from 'mongoose';

export const app = express();

if (process.env.NODE_ENV === 'test') {
  process.env.MONGO_URI =
    'mongodb+srv://tranvancong:88888888@demo-t9spu.mongodb.net/btlxml?retryWrites=true'
  process.env.PORT = 2001;
}

dotenv.config();

mongoose.Promise = global.Promise;
mongoose
  .connect(process.env.MONGO_URI, {
    useCreateIndex: true,
    useNewUrlParser: true
  })
  .then(() => {
    console.log('connected to database');
  })
  .catch(error => console.log('MONGO CONNECT FAIL', error));

const appPort = process.env.PORT;
// export const News = mongoose.model('data_final_json', newsSchema);
app.set('view engine', 'ejs');
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico'))); // eslint-disable-line
app.use(express.static(path.join(__dirname, 'public'))); // eslint-disable-line
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

export const server = Server(app);

// Add headers
app.use((req, res, next) => {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');
  // Request methods you wish to allow
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, OPTIONS, PUT, PATCH, DELETE'
  );
  // Request headers you wish to allow
  res.setHeader(
    'Access-Control-Allow-Headers',
    'X-Requested-With,content-type,Authorization,Origin,Accept'
  );
  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);
  // Pass to next layer of middleware
  next();
});

app.use('/', routes);

server.listen(appPort);

export default app;
