import '@babel/polyfill';
import 'chai/register-expect';
import chai from 'chai';
import chaiHttp from 'chai-http';
import { app } from '../app';
import { populateData } from './seed/helperTest';
import * as seed from './seed/seed';
import user from '../models/user';
import apartment from '../models/apartment';
import faq from '../models/faq';
import promotion from '../models/promotion';
import service from '../models/service';
import supplier from '../models/supplier';
import news from '../models/news';
import project from '../models/project';
import booking from '../models/booking';
import contract from '../models/contract';
import feedback from '../models/feedback';

chai.use(chaiHttp);

beforeEach(done => populateData(done, seed.users, user));
beforeEach(done => populateData(done, seed.apartments, apartment));
beforeEach(done => populateData(done, seed.faqs, faq));
beforeEach(done => populateData(done, seed.promotions, promotion));
beforeEach(done => populateData(done, seed.services, service));
beforeEach(done => populateData(done, seed.suppliers, supplier));
beforeEach(done => populateData(done, seed.news, news));
beforeEach(done => populateData(done, seed.apartments, apartment));
beforeEach(done => populateData(done, seed.projects, project));
beforeEach(done => populateData(done, seed.bookings, booking));
beforeEach(done => populateData(done, seed.contracts, contract));
beforeEach(done => populateData(done, seed.feedbacks, feedback));

it('test', () => {
  expect(2).to.equal(2);
});
