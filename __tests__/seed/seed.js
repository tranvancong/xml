import mongoose from 'mongoose';
import uuidv4 from 'uuid/v4';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import bcrypt from 'bcryptjs';
import { generatorToken } from '../../controllers/auths';

// DEF ID
export const apartmentsId = [
  mongoose.Types.ObjectId().toHexString(),
  mongoose.Types.ObjectId().toHexString()
];

export const projectsId = [
  mongoose.Types.ObjectId().toHexString(),
  mongoose.Types.ObjectId().toHexString()
];

export const bookingsId = [
  mongoose.Types.ObjectId().toHexString(),
  mongoose.Types.ObjectId().toHexString()
];

// ADMIN
const adminBaseToken = uuidv4();
const adminId = mongoose.Types.ObjectId().toHexString();

export const admin = {
  _id: adminId,
  email: 'duylinh196tb@gmail.com',
  name: 'duylinh196tb@gmail.com',
  password: bcrypt.hashSync('123456', 8),
  baseToken: adminBaseToken,
  token: generatorToken(adminId, adminBaseToken),
  role: 0
};

export const users = [
  { ...admin },
  {
    _id: mongoose.Types.ObjectId().toHexString(),
    name: 'test 1',
    password: bcrypt.hashSync('1234561', 8),
    baseToken: uuidv4(),
    role: 2,
    bookings: [bookingsId[0]]
  },
  {
    _id: mongoose.Types.ObjectId().toHexString(),
    name: 'test 2',
    password: bcrypt.hashSync('1234562', 8),
    baseToken: uuidv4(),
    role: 2,
    bookings: [bookingsId[1]]
  }
];

// FAQ
export const faqs = [
  {
    _id: mongoose.Types.ObjectId().toHexString(),
    question: 'question test',
    answer: 'answer test'
  },
  {
    _id: mongoose.Types.ObjectId().toHexString(),
    question: 'question test',
    answer: 'answer test'
  },
  {
    _id: mongoose.Types.ObjectId().toHexString(),
    question: 'question test',
    answer: 'answer test'
  },
  {
    _id: mongoose.Types.ObjectId().toHexString(),
    question: 'question test',
    answer: 'answer test'
  },
  {
    _id: mongoose.Types.ObjectId().toHexString(),
    question: 'question test',
    answer: 'answer test'
  },
  {
    _id: mongoose.Types.ObjectId().toHexString(),
    question: 'question test',
    answer: 'answer test'
  }
];

// SERVICES
export const services = [
  {
    _id: mongoose.Types.ObjectId().toHexString(),
    thumbnail: 'question 1',
    detail: '3',
    deteAndTime: 'thu 6 ngay 13'
  },
  {
    _id: mongoose.Types.ObjectId().toHexString(),
    thumbnail: 'question 2',
    detail: '1',
    deteAndTime: 'thu 7 ngay 14'
  }
];
// SUPPLIERS
export const suppliers = [
  {
    _id: mongoose.Types.ObjectId().toHexString(),
    name: 'supplier 1',
    about: 'about 1',
    star: 4,
    contact: {
      numberphone: '0983284',
      message: 'link'
    }
  },
  {
    _id: mongoose.Types.ObjectId().toHexString(),
    name: 'supplier 2',
    about: 'about 2',
    star: 4,
    contact: {
      numberphone: '0983284',
      message: 'link'
    }
  }
];

// PROMOTIONS
export const promotions = [
  {
    _id: mongoose.Types.ObjectId().toHexString(),
    title: 'title 1',
    thumbnail: 'dasds',
    content: 'News 1',
    date: new Date()
  },
  {
    _id: mongoose.Types.ObjectId().toHexString(),
    title: 'title 2',
    name: 'News 2',
    thumbnail: 'dasds',

    date: new Date()
  }
];

// FEEDBACKS
export const feedbacks = [
  {
    _id: mongoose.Types.ObjectId().toHexString(),
    title: 'title 1',
    content: 'dasds'
  },
  {
    _id: mongoose.Types.ObjectId().toHexString(),
    title: 'title 2',
    content: 'dasds'
  }
];

// NEWS
export const news = [
  {
    _id: mongoose.Types.ObjectId().toHexString(),
    title: 'title 1',
    content: 'News 1'
  },
  {
    _id: mongoose.Types.ObjectId().toHexString(),
    title: 'title 2',
    content: 'News 2'
  }
];

// APARTMENTS

export const apartments = [
  {
    _id: apartmentsId[0],
    code: 'dsa',
    name: 'Apartment 1',
    location: 'String',
    area: 321,
    cost: 321,
    project: projectsId[0],
    building: 'buildingsId[0]',
    bookings: [bookingsId[0], bookingsId[1]]
  },
  {
    _id: apartmentsId[1],
    code: 'dsa',
    name: 'Apartment 2',
    location: 'String',
    area: 321,
    cost: 321,
    project: projectsId[1],
    building: 'buildingsId[1]'
  }
];

// PROJECTS

export const projects = [
  {
    _id: projectsId[0],
    name: 'Project 1',
    address: 'String',
    location: 'String',
    thumbnail: 'String',
    apartments: [apartmentsId[0]]
  },
  {
    _id: projectsId[1],
    name: 'Project 2',
    address: 'String',
    location: 'String',
    thumbnail: 'String',
    apartments: [apartmentsId[1]]
  }
];

// BOOKINGS
export const bookings = [
  {
    _id: bookingsId[0],
    apartment: apartmentsId[0],
    dateStart: new Date(2018, 11, 24, 10, 33, 30, 0),
    dateEnd: new Date(2018, 11, 26, 10, 33, 30, 0),
    user: users[0]._id
  },
  {
    _id: bookingsId[1],
    apartment: apartmentsId[1],
    dateStart: new Date(2018, 11, 27, 10, 33, 30, 0),
    dateEnd: new Date(2018, 11, 28, 10, 33, 30, 0),
    user: users[1]._id
  }
];

// CONTRACTS
export const contracts = [
  {
    _id: mongoose.Types.ObjectId().toHexString(),
    dateStart: new Date(2018, 11, 24, 10, 33, 30, 0),
    dateEnd: new Date(2018, 11, 26, 10, 33, 30, 0),
    services: ['SV1', 'SV2'],
    booking: bookingsId[0]
  },
  {
    _id: mongoose.Types.ObjectId().toHexString(),
    dateStart: new Date(2018, 11, 27, 10, 33, 30, 0),
    dateEnd: new Date(2018, 11, 28, 10, 33, 30, 0),
    services: ['SV1', 'SV2'],
    booking: bookingsId[1]
  }
];
