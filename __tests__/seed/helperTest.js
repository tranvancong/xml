import chai from 'chai';
import { it } from 'mocha';
import chaiHttp from 'chai-http';

const keyToken = 'Authorization';

const { expect } = chai;
chai.use(chaiHttp);
export const hostApp = `http://localhost:${process.env.PORT}/api/v1`;

export const checkRes = (err, res, done, code = 200) => {
  if (err) done(err);
  expect(res).to.have.status(200);
  expect(res.body.code).to.equal(code);
};

export const populateData = (done, data, model) => {
  model
    .deleteMany({})
    .then(() => model.insertMany(data))
    .then(() => done());
};

export const testPost = (
  { dataSeed, model, token, url, host = hostApp },
  data,
  cb
) => {
  it('it should post data', done => {
    chai
      .request(host)
      .post(url)
      .set({ [keyToken]: token })
      .send(data)
      .end(async (err, res) => {
        try {
          checkRes(err, res, done);
          cb && (await cb(res.body));
          const dataTest = await Promise.all([
            model.find(),
            model.findById(res.body.data._id)
          ]);
          expect(dataTest[0]).to.have.lengthOf(dataSeed.length + 1);
          done();
        } catch (error) {
          done(error);
        }
      });
  });
};

export const testGet = (
  { dataSeed, model, token, url, host = hostApp },
  cb1,
  cb2
) => {
  it('it should get all data', done => {
    chai
      .request(host)
      .get(url)
      .set({ [keyToken]: token })
      .end(async (err, res) => {
        try {
          checkRes(err, res, done);
          cb1 && (await cb1(res.body));
          await model.find().then(d => {
            expect(d).to.have.lengthOf(dataSeed.length);
            done();
          });
        } catch (error) {
          done(error);
        }
      });
  });

  it('it should get data by id', done => {
    chai
      .request(host)
      .get(`${url}/${dataSeed[0]._id}`)
      .set({ [keyToken]: token })
      .end(async (err, res) => {
        try {
          checkRes(err, res, done);
          cb2 && (await cb2(res.body));
          await model.findById(dataSeed[0]._id).then(d => {
            expect(d).to.exist;
            done();
          });
        } catch (error) {
          done(error);
        }
      });
  });
};

export const testDelete = (
  { dataSeed, model, token, url, host = hostApp, body },
  cb
) => {
  it('it should delete by id', done => {
    chai
      .request(host)
      .delete(`${url}/${dataSeed[0]._id}`)
      .set({ [keyToken]: token })
      .send(body)
      .end(async (err, res) => {
        try {
          checkRes(err, res, done);
          cb && (await cb(res.body));
          await model.findById(dataSeed[0]._id).then(d => {
            expect(dataSeed[0]._id).to.equal(res.body.data._id);
            expect(d).to.not.exist;
            done();
          });
        } catch (error) {
          done(error);
        }
      });
  });
};

export const testUpdate = (
  { dataSeed, model, token, url, host = hostApp },
  data,
  cb
) => {
  it('it should update by id', done => {
    chai
      .request(host)
      .patch(`${url}/${dataSeed[0]._id}`)
      .set({ [keyToken]: token })
      .send(data)
      .end(async (err, res) => {
        try {
          checkRes(err, res, done);
          cb && (await cb(res.body));
          done();
        } catch (error) {
          done(error);
        }
      });
  });
};

const returnMethod = (host, url, customUrl, method) => {
  switch (method.toLowerCase()) {
    case 'get':
      return chai.request(host).get(`${url}/${customUrl}`);
    case 'post':
      return chai.request(host).post(`${url}/${customUrl}`);
    case 'put':
      return chai.request(host).put(`${url}/${customUrl}`);
    case 'patch':
      return chai.request(host).patch(`${url}/${customUrl}`);
    case 'delete':
      return chai.request(host).delete(`${url}/${customUrl}`);
    default:
      break;
  }
};

export const testOther = (
  { dataSeed, model, token, url, host = hostApp },
  {
    title = '',
    code,
    method = 'get',
    customUrl = '',
    data = null,
    customToken = null
  },
  callback
) => {
  it(title, done => {
    returnMethod(host, url, customUrl, method)
      .set({ [keyToken]: customToken || token })
      .send(data)
      .end(async (err, res) => {
        try {
          checkRes(err, res, done, code);
          callback && (await callback(res.body));
          done();
        } catch (error) {
          done(error);
        }
      });
  });
};
