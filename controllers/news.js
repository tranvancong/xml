import News from '../models/news';
import { CODE } from '../constants/index';

const getAllNews = async (req, res) => {
  const { page } = req.query;
  const quantity = Number(req.query.quantity);
  const totalNumberOfNews = await News.countDocuments();

  const listNews = await News.find()
    .select('title time url content')
    .sort('-createdAt')
    .limit(quantity)
    .skip((Number(page) - 1) * quantity);

  if (listNews) {
    res.json({
      code: CODE.SUCCESS,
      message: 'Lấy danh sách tin tức thành công!',
      data: listNews,
      page: Number(page),
      totalPage: Math.ceil(totalNumberOfNews / quantity)
    });
  }

  return res.json({
    code: CODE.OBJECT_NOT_FOUND,
    message: "Không có dữ liệu"
  });
};

export default {
  getAllNews
};
