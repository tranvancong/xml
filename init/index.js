import User from '../models/user';
import Config from '../models/config';
import config from '../config';
import uuidv4 from 'uuid/v4';
import bcrypt from 'bcryptjs';

export const initAdmin = async () => {
  try {
    const [adminAccount, vinConfig] = await Promise.all([
      User.findOne({ email: config.adminEmail }),
      Config.findOne({ key: 'VIN_LEASING' })
    ]);

    if (!adminAccount) {
      const adminAccount = new User({
        phone: config.adminPhone,
        email: config.adminEmail,
        password: bcrypt.hashSync(config.adminPassword, 8),
        name: 'Admin',
        baseToken: uuidv4(),
        role: 0
      });
      await adminAccount.save();
    }
    if (!vinConfig) {
      await Config.create({ key: 'VIN_LEASING' });
    }
  } catch (e) {
    console.log('===============WARNNING================ ');
    console.log('       ADMIN ACCOUNT CREATE ERROR       ');
    console.log('======================================= ');
  }
};

export default {
  initAdmin
};
