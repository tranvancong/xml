export default {
  dbUrl: '',
  serverUrl: '',

  sessionSecret: '',
  appSecret: '',
  clientSecret: '',

  fbUrl: '',
  accountKitUrl: '',

  emailVerify: '',
  passwordVerify: '',
  hostMail: '',
  portMail: 587,

  adminEmail: '',
  adminPassword: '',
  adminPhone: ''
};
