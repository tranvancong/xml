import mongoose, { Schema } from 'mongoose';

const newsSchema = new Schema({
  title: String,
  time: String,
  content: String,
  url: String
})

export default mongoose.model('News', newsSchema, 'data_final_json');